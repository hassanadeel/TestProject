package com.example.hassan.westpestproject.view;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by HP 1 on 2/3/2016.
 */
public class ViewHolder {

    TextView heading;
    TextView summary;
    ImageView imageView1;
    TextView temperature;

    public TextView getHeading() {
        return heading;
    }

    public void setHeading(TextView heading) {
        this.heading = heading;
    }

    public TextView getSummary() {
        return summary;
    }

    public void setSummary(TextView summary) {
        this.summary = summary;
    }

    public ImageView getImageView1() {
        return imageView1;
    }

    public void setImageView1(ImageView imageView1) {
        this.imageView1 = imageView1;
    }
    public TextView getTemperature() {
        return temperature;
    }

    public void setTemperature(TextView temperature) {
        this.temperature = temperature;
    }
}
