package com.example.hassan.westpestproject.activity;

import android.Manifest;
import android.app.ListActivity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.hassan.westpestproject.R;
import com.example.hassan.westpestproject.adapter.WeatherDataAdapter;
import com.example.hassan.westpestproject.com.example.hassan.westpestproject.util.NetworkUtils;
import com.example.hassan.westpestproject.constants.IConstants;
import com.example.hassan.westpestproject.model.WeatherData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.location.Criteria;
import android.view.View;
import android.widget.ListView;

public class ListViewActivity extends ListActivity {

    private LocationManager locationManager;
    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Criteria c = new Criteria();

        String provider = locationManager.getBestProvider(c, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        location = locationManager.getLastKnownLocation(provider);
        if(location != null)
        {
            double lng= location.getLongitude();
            double lat= location.getLatitude();
            Log.d("RESULT", ""+lng);
            Log.d("RESULT", ""+lat);

            String url = IConstants.WEATHER_API_URL + lat + "," + lng;
            Log.d("RESULT", "======================================================URL =========================:" + url );
            new DownloadWebData().execute(url);

        }
        else
        {
            Log.d("RESULT", "No Provider");
            Log.d("RESULT", "No Provider");
            return;
        }
    }

    protected void onListItemClick (ListView l, View v, int position, long id){
        Log.d("RESULT", "onListItemClick Clicked" + id);
    }

    class DownloadWebData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                return  new NetworkUtils(params[0]).downloadUrl();
            } catch (IOException e) {
                return "Unable to fetch data from the given URL::" + params[0] + ":" + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("DEBUG", "The result is: " + result);

            JSONObject jObject = null;
            List<WeatherData> userDataList = new ArrayList<WeatherData>();

            try{
                 //result = JSONObject. (result);
                jObject = new JSONObject(result);
                JSONObject currently = jObject.getJSONObject("currently");
                JSONObject minutely = null;
                JSONObject hourly = jObject.getJSONObject("hourly");
                JSONObject daily = jObject.getJSONObject("daily");

                if(jObject.has("minutely")){
                    minutely = jObject.getJSONObject("minutely");
                    String summaryMts = minutely.getString("summary");
                    if(summaryMts != null && summaryMts.length() > 0) {
                        WeatherData dataMts =  new WeatherData();
                        dataMts.setSummary(summaryMts);
                        dataMts.setIcon(minutely.getString("icon"));
                        dataMts.setHeading("Minutely");
                        userDataList.add(dataMts);
                    }
                }

                WeatherData data =  new WeatherData();
                String summary = currently.getString("summary");
                if(summary != null && summary.length() > 0) {
                    data.setSummary(summary);
                    data.setIcon(currently.getString("icon"));
                    data.setHeading("Currently");
                    data.setTemperature(currently.getString("temperature") + " C");
                    userDataList.add(data);
                }

                WeatherData dataHourly =  new WeatherData();
                String hourlySummary = hourly.getString("summary");
                if(hourlySummary != null && hourlySummary.length() > 0) {
                    dataHourly.setSummary(hourlySummary);
                    dataHourly.setIcon(hourly.getString("icon"));
                    dataHourly.setHeading("Hourly");
                    userDataList.add(dataHourly);
                }

                WeatherData dataDaily = new WeatherData();
                String dataDailySummary = daily.getString("summary");
                if(dataDailySummary != null && dataDailySummary.length() > 0) {
                    dataDaily.setSummary(dataDailySummary);
                    dataDaily.setIcon(daily.getString("icon"));
                    dataDaily.setHeading("Daily");
                    userDataList.add(dataDaily);
                }
                setListAdapter(new WeatherDataAdapter(ListViewActivity.this, R.layout.listview_row_layout, userDataList));
            }catch(JSONException e){
                Log.d("DEBUG", "Exception while parsing result into json:" + e.getMessage());
            }
        }
    }
}
