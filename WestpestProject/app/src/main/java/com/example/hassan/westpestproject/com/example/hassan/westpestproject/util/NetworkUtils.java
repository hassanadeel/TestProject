package com.example.hassan.westpestproject.com.example.hassan.westpestproject.util;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by HP 1 on 2/2/2016.
 */
public class NetworkUtils {

    private String myurl = "";

    public NetworkUtils(String myurl){
        this.myurl = myurl;
    }

    public String downloadUrl() throws IOException {
        InputStream is = null;
        int len = 25000;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("DEBUG", "The response is: " + response);
            Log.d("DEBUG", "The response is: " + response);
            is = conn.getInputStream();

            String contentAsString = readData(is, len);

            Log.d("DEBUG", "The response is: " + contentAsString);
            return contentAsString;

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    public String readData(InputStream stream, int len) throws IOException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }
}
