package com.example.hassan.westpestproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hassan.westpestproject.R;
import com.example.hassan.westpestproject.model.WeatherData;
import com.example.hassan.westpestproject.view.ViewHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HP 1 on 2/2/2016.
 */
public class WeatherDataAdapter extends ArrayAdapter<WeatherData> {

    private  ViewHolder viewHolder = null;
    private static Map<String, Integer> imageMap = new HashMap<String, Integer>();
    static
    {
        imageMap.put("partly-cloudy-night", R.drawable.partly_cloudy_night);
        imageMap.put("rain", R.drawable.rain);
        imageMap.put("clear-day", R.drawable.sunny);
        imageMap.put("clear-night", R.drawable.clear_night);
        imageMap.put("partly-cloudy-day",R.drawable.partly_cloudy_day);
        imageMap.put("sunny", R.drawable.sunny);
    }
    public WeatherDataAdapter(Context context, int resource, List<WeatherData> dataList) {
        super(context, resource, dataList);
    }

    public WeatherDataAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {

            viewHolder = new ViewHolder();
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_row_layout, null);

            ImageView imageView1 = (ImageView) v.findViewById(R.id.imageView1);
            viewHolder.setImageView1(imageView1);

            TextView heading = (TextView) v.findViewById(R.id.heading);
            viewHolder.setHeading(heading);

            TextView summary = (TextView) v.findViewById(R.id.textView2);
            viewHolder.setSummary(summary);

            TextView temp = (TextView) v.findViewById(R.id.temperature);
            viewHolder.setTemperature(temp);
        }

        WeatherData weatherData = getItem(position);
        viewHolder.getHeading().setText(weatherData.getHeading());
        viewHolder.getSummary().setText(weatherData.getSummary());
        viewHolder.getTemperature().setText(weatherData.getTemperature());

        String str = "sunny";
        if(weatherData.getIcon() != null || weatherData.getIcon().length() > 0){
            str = weatherData.getIcon();
        }
        viewHolder.getImageView1().setImageResource(imageMap.get(str));
        return v;
    }
}
